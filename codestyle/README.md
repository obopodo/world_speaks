# Links
- See https://pre-commit.com for more information
- See https://pre-commit.com/hooks.html for more hooks

# Setup

pre-commit included in pyproject.toml

# Usage

## Find pre-commit binary

```shell
which pre-commit
```

## Manually update files

1. `pre-commit-config.yaml`:
   - use needed hooks, remove those not relevant
2. Create `pre-commit-run.sh` from `pre-commit-run.sh.example`:
   - path to pre-commit binary
   - `-c CONFIG` path to pre-commit yaml config file
   - `--color {auto,always,never}` whether to use color in output
   - `-v` produce hook output
   - `--files [FILES [FILES ...]]` specific filenames to run hooks on
   - For other options see: https://pre-commit.com/#cli, https://pre-commit.com/#pre-commit-run
   - Example (see pre-commit-run.sh.example):
   ```shell
   /some_path/pre-commit run -c /other_path/pre-commit-config.yaml --color always -v --files /some_other_path/src/* /some_other_path/src/data/*
   ```

## Run

```shell
sh pre-commit-run.sh
```

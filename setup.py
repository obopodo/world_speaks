from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Image captioning tool',
    author='World Speaks Ltd.',
    license='BSD-3',
)

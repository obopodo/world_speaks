"""Train model."""
import torch
from torch.utils.data import DataLoader
from tqdm.auto import tqdm
from transformers import BlipForConditionalGeneration

from src import ImageCaptioningDataset


def train_model(
    train_dataset: ImageCaptioningDataset,
    epochs: int,
    batch_size: int,
    adamw_lr: float = 5e-5,
    save_directory: str = "",
) -> BlipForConditionalGeneration:
    """
    Train model.

    #TODO add return desription

    Args:
        train_dataset (ImageCaptioningDataset): Custom dataset instance for model training.
        epochs (int): Number of training iterations.
        batch_size (int): Number of pairs image-text in a batch
        adamw_lr (float, optional): Optimizer's learning rate (`lr` parameter of
            `torch.optim.AdamW`). Defaults to 5e-5.
        save_directory (str, optional): _description_. Defaults to "".

    Returns
    -------
    BlipForConditionalGeneration: _description_.

    """
    model = BlipForConditionalGeneration.from_pretrained(
        "Salesforce/blip-image-captioning-base"
    )
    optimizer = torch.optim.AdamW(model.parameters(), lr=adamw_lr)
    train_dataloader = DataLoader(train_dataset, shuffle=True, batch_size=batch_size)

    device = "cuda" if torch.cuda.is_available() else "cpu"
    model.to(device)

    model.train()

    for epoch in tqdm(range(epochs), leave=False):
        print("Epoch:", epoch)
        for batch in train_dataloader:
            input_ids = batch.pop("input_ids").to(device)
            pixel_values = batch.pop("pixel_values").to(device)

            outputs = model(
                input_ids=input_ids, pixel_values=pixel_values, labels=input_ids
            )

            loss = outputs.loss

            print("Loss:", loss.item())

            loss.backward()

            optimizer.step()
            optimizer.zero_grad()

    if save_directory:
        model.save_pretrained(save_directory)
    return model

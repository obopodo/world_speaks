"""Make prediction."""
from typing import List

import torch
from transformers import BlipForConditionalGeneration

from src import PROCESSOR, ImageCaptioningDataset


def predict(
    model: BlipForConditionalGeneration, test_dataset: ImageCaptioningDataset
) -> List[str]:
    """
    Make prediction.

    #TODO add return desription

    Args:
        model (BlipForConditionalGeneration): _description_.
        test_dataset (ImageCaptioningDataset): _description_.

    Returns
    -------
    List[str]: _description_.

    """
    device = "cuda" if torch.cuda.is_available() else "cpu"
    captions = []
    # TODO implement correct iteration through test_dataset element with class method
    for data in test_dataset:
        image = data["image"]
        inputs = PROCESSOR(images=image, return_tensors="pt").to(device)
        pixel_values = inputs.pixel_values
        generated_ids = model.generate(pixel_values=pixel_values, max_length=50)
        generated_caption = PROCESSOR.batch_decode(
            generated_ids, skip_special_tokens=True
        )[0]
        captions.append(generated_caption)
    return captions

"""Data init."""

from .dataset_loader import PROCESSOR, init_dataset
from .image_captioning_dataset import ImageCaptioningDataset

__all__ = ["update_captions", "PROCESSOR", "init_dataset", "ImageCaptioningDataset"]

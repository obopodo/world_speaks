"""Update image captions."""
import sys
from pathlib import Path

import pandas as pd


def update_captions(data_folder: str, save_folder: str | None = None) -> Path:
    """
    Update image captions.

    #TODO add return desription

    Args:
        data_folder (str): Path to folder with file results.csv.
        save_folder (str | None, optional): Path to folder to save new metadata.csv file.
            Defaults to None - saves to data_folder.

    Returns
    -------
    str: _description_

    """
    if save_folder is None:
        save_folder = data_folder

    data_folder_path: Path = Path(data_folder)
    save_folder_path: Path = Path(save_folder)
    captions = pd.read_table(data_folder_path / "results.csv", sep="|")
    captions.columns = ["file_name", "n", "text"]

    # fix parsing of spoiled row in original csv file
    has_na = captions.isna().any()
    if has_na.any():
        print("Data has NaNs")
        print(captions.iloc[[19999], :])
        captions.iloc[19999, 2] = captions.iloc[19999, 1].strip("4 ")
        print("\nFixed!")
        print(captions.iloc[[19999], :])

    # make table with 6 columns: file_name and 5 corresponding captions
    captions = captions[["file_name", "text"]]
    captions_grouped = captions.groupby("file_name").apply(lambda x: x["text"].tolist())
    new_captions = pd.DataFrame({"file_name": captions_grouped.index})
    new_captions[[f"text_{i}" for i in range(5)]] = pd.DataFrame(
        captions_grouped.tolist(), columns=[f"text_{i}" for i in range(5)]
    )

    # save updated captions to the folder, containing images
    save_path = save_folder_path / "metadata.csv"
    new_captions.to_csv(save_path, index=False)
    print(f"\nUpdated captions were saved to\n{save_path}")
    return save_path


if __name__ == "__main__":
    DATA_FOLDER = sys.argv[1]
    _ = update_captions(data_folder=DATA_FOLDER)

"""ImageCaptioningDataset class."""
from typing import Dict

import numpy as np
from torch.utils.data import Dataset


class ImageCaptioningDataset(Dataset):
    """ImageCaptioningDataset class."""

    def __init__(self, dataset, processor, n_captions=5):
        """Initialize class instance."""
        self.dataset = dataset
        self.processor = processor
        self._n_captions = n_captions
        self._idx_mapping = self._init_idx_mapping()

    def _init_idx_mapping(self) -> Dict[int, int]:
        """
        Permute identical images with different captions.

        It seems like a bad idea to pass to the model 5 identical images in a row.
        #TODO check if this method helps to improve model quality

        #TODO add return desription

        Returns
        -------
        Dict[int, int]: _description_.

        """
        ids = np.arange(len(self), dtype=int)
        np.random.seed(len(self))
        ids_permuted = np.random.permutation(ids)
        return dict(zip(ids, ids_permuted))

    def __len__(self):
        """Return required dataset length."""
        return len(self.dataset) * 5

    def __getitem__(self, idx):
        """Get encoding dictionary."""
        true_idx = self._idx_mapping[idx]
        idx = int(true_idx // 5)
        text_idx = int(true_idx % 5)
        item = self.dataset[idx]
        text = item[f"text_{text_idx}"]
        encoding = self.processor(
            images=item["image"], text=text, padding="max_length", return_tensors="pt"
        )
        # remove batch dimension
        encoding = {k: v.squeeze() for k, v in encoding.items()}
        return encoding

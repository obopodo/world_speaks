"""Example script for dataset generation."""
import logging
import os
import shutil
from pathlib import Path

import click
import pandas as pd
from sklearn.model_selection import train_test_split
from update_captions import update_captions

PROJECT_FOLDER = Path(__file__).parents[2]
NEEDED_SUBFOLDERS = (
    "data",
    "data/raw",
    "data/processed",
    "data/processed/train",
    "data/processed/test",
)


def make_data_folders() -> None:
    """Make needed data folders."""
    logger = logging.getLogger(__name__)
    logger.info("Make data folders...")

    for subfolder in NEEDED_SUBFOLDERS:
        os.makedirs(PROJECT_FOLDER / Path(subfolder), exist_ok=True)


def split_data(raw_data_folder: str) -> None:
    """Split images and their processed metadata into train and test."""
    logger = logging.getLogger(__name__)
    logger.info("Split data into train and test...")

    raw_data_path: Path = Path(raw_data_folder)
    src_folder = raw_data_path / "flickr30k_images"
    train_folder = Path("data/processed/train/")
    test_folder = Path("data/processed/test/")

    filenames = sorted(os.listdir(src_folder))
    new_captions = pd.read_csv(raw_data_path / "metadata.csv")
    names_train, names_test = train_test_split(filenames, shuffle=False, test_size=0.1)
    # copy images to train/test folders
    for fname in names_train:
        shutil.copy(src_folder / fname, train_folder)
    for fname in names_test:
        shutil.copy(src_folder / fname, test_folder)

    # add metadata files inside train/test folders
    new_captions[new_captions.file_name.isin(names_train)].to_csv(
        train_folder / "metadata.csv", index=False
    )
    new_captions[new_captions.file_name.isin(names_test)].to_csv(
        test_folder / "metadata.csv", index=False
    )


@click.command()
@click.argument(
    "raw_data_path",
    type=click.Path(exists=True),
)
def main(raw_data_path: str = "."):
    """Make train/test data."""
    logger = logging.getLogger(__name__)
    logger.info("making final data set from raw data")
    logger.info(f"raw_data_path: {raw_data_path}")

    make_data_folders()
    logger.info("Update captions")
    _ = update_captions(raw_data_path)
    split_data(raw_data_path)


if __name__ == "__main__":
    LOG_FMT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=LOG_FMT)

    main()

"""Dataset loader."""

from typing import Tuple

from datasets import load_dataset
from transformers import AutoProcessor

from .image_captioning_dataset import ImageCaptioningDataset

PROCESSOR = AutoProcessor.from_pretrained("Salesforce/blip-image-captioning-base")


def init_dataset(
    images_folder: str,
    subfolder: str | None = None,
    force_redownload: bool = False,
) -> Tuple[ImageCaptioningDataset, ImageCaptioningDataset]:
    """
    Initialize `ImageCaptioningDataset`s for train and test.

    #TODO add return desription

    Args:
        images_folder (str): Path to folder containing required images and metadata.csv.
        subfolder (str | None, optional): Path to folder containing specific data fold,
            e.g. train. Defaults to None.
        force_redownload (bool, optional): Whether should redownload dataset or use the
            cached one. Defaults to False.

    Returns
    -------
    Tuple[ImageCaptioningDataset, ImageCaptioningDataset]: _description_.

    """
    download_mode = "force_redownload" if force_redownload else None

    dataset = load_dataset(
        path=images_folder, download_mode=download_mode, data_dir=subfolder
    )

    dataset = ImageCaptioningDataset(dataset=dataset, processor=PROCESSOR, n_captions=5)
    return dataset
